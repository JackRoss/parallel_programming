#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#define N 10

int main(int argc, char *argv[]) {

  int npes = 1;
  int rank = 0;
  FILE *file = fopen("mpi_identity.txt", "w");
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);
  int rest = N % npes;
  int n_loc = N / npes;
  int local_start;

  if (rank < rest) {
    n_loc = n_loc + 1;
    local_start = rank * (n_loc);
  } else {
    local_start = rest + (rank * n_loc);
  }

  int *local_mat = (int *)malloc(N * n_loc * sizeof(int));

  for (int i = 0; i < n_loc; i++) {
    local_mat[(N + 1) * i + local_start] = 1;
  }

  if (rank != 0) {
    MPI_Send(local_mat, N * n_loc, MPI_INT, 0, 101, MPI_COMM_WORLD);
  }
  if (rank == 0) {
    for (int j = 0; j < n_loc; j++) {
      for (int k = 0; k < N; k++) {
        fprintf(file, "%d ", local_mat[j * N + k]);
      }
      fprintf(file, "\n");
    }

    int *new_local_mat = (int *)malloc(N * n_loc * sizeof(int));
    for (int i = 1; i < npes; i++) {
      if (rest != 0 && i == rest) {
        n_loc = n_loc - 1;
      }
      MPI_Recv(new_local_mat, N * n_loc, MPI_INT, i, 101, MPI_COMM_WORLD,
               MPI_STATUS_IGNORE);

      for (int j = 0; j < n_loc; j++) {
        for (int k = 0; k < N; k++) {
          fprintf(file, "%d ", new_local_mat[j * N + k]);
        }
        fprintf(file, "\n");
      }
    }
  }
  MPI_Finalize();
  fclose(file);
  return 0;
}
