#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>
#define TILE 32 //to be changed accordingly to the threads number of interest
#define N 8192


double seconds()
{

  struct timeval tmp;
  double sec;
  gettimeofday( &tmp, (struct timezone *)0 );
  sec = tmp.tv_sec + ((double)tmp.tv_usec)/1000000.0;
  return sec;

}

__global__ void fast_transpose(const double *mat_in,double *mat_out) {
  __shared__ double tile[TILE][TILE];

    size_t x = blockIdx.x * blockDim.x + threadIdx.x;
    size_t y = blockIdx.y * blockDim.y + threadIdx.y;
    size_t in = x + y*N;



       tile[threadIdx.y][threadIdx.x] = mat_in[in];

    __syncthreads();
    x = blockIdx.y * blockDim.y + threadIdx.x;
    y = blockIdx.x * blockDim.x + threadIdx.y;
    size_t out = x+y*N;
    mat_out[out] = tile[threadIdx.x][threadIdx.y];

}
int main(){
  double *mat_in, *mat_out;
  double *mat_in_d, *mat_out_d;
  double time;
  size_t size = N*N * sizeof( double );


  // allocate device copies of a, b, c
  cudaMalloc( (void** )&mat_in_d, size );
  cudaMalloc( (void** )&mat_out_d, size );
  mat_in = (double*)malloc( size );
  mat_out = (double*)malloc( size );
  for(size_t i=0;i<N;i++){
      for(size_t j=0;j<N;j++){
        mat_in[i*N+j]=i*n+j;

      }
  }

  dim3 grid(256, 256);
  dim3 block(32,32);

  cudaMemcpy( mat_in_d, mat_in, size, cudaMemcpyHostToDevice );
  time=seconds();
  fast_transpose<<< grid, block >>>( mat_in_d, mat_out_d);
  cudaDeviceSynchronize();
  time=seconds()-time;
  cudaMemcpy(mat_out, mat_out_d,  size, cudaMemcpyDeviceToHost );

  printf("the time elapsed using 65536 blocks with 1024 threads = %lf\n",time);
/*  for(int i=0;i<10;i++){
      for(int j=0;j<10;j++){
        printf("%lf ",mat_out[i*N+j] );

  }
  printf("\n" );
}
for(int i=0;i<10;i++){
    for(int j=0;j<10;j++){
        printf("%lf ",mat_in[i*N+j] );
      }
      printf("\n" );
}*/

  printf("\n" );
  printf("\n" );
  dim3 grid1(512, 512);
  dim3 block1(16,16);


  cudaMemcpy( mat_in_d, mat_in, size, cudaMemcpyHostToDevice );
  time=seconds();
  fast_transpose<<< grid1, block1 >>>( mat_in_d, mat_out_d);
  cudaDeviceSynchronize();
  time=seconds()-time;
  cudaMemcpy(mat_out, mat_out_d,  size, cudaMemcpyDeviceToHost );

  printf("the time elapsed using 512*512 blocks with 256 threads = %lf\n",time);


  dim3 grid2(1024, 1024);
  dim3 block2(8,8);


  cudaMemcpy( mat_in_d, mat_in, size, cudaMemcpyHostToDevice );
  time=seconds();
  fast_transpose<<< grid2, block2 >>>( mat_in_d, mat_out_d);
  cudaDeviceSynchronize();
  time=seconds()-time;
  cudaMemcpy(mat_out, mat_out_d,  size, cudaMemcpyDeviceToHost );

  printf("the time elapsed using 1024*1024 blocks with 64 threads = %lf\n",time);
  free( mat_in ); free( mat_out );
  cudaFree( mat_in_d );
  cudaFree( mat_out_d );
}
