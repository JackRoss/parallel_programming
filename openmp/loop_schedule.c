#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
void print_usage(int *a, int N, int nthreads) {

  int tid, i;
  for (tid = 0; tid < nthreads; ++tid) {

    fprintf(stdout, "%d: ", tid);

    for (i = 0; i < N; ++i) {

      if (a[i] == tid)
        fprintf(stdout, "*");
      else
        fprintf(stdout, " ");
    }
    printf("\n");
  }
}

int main(int argc, char *argv[]) {
  const int N = 100;
  int a[N];
  int thread_id = 0;
  int nthreads = 1;

  for (int i = 0; i < N; ++i) {
    a[i] = thread_id;
  }
  print_usage(a, N, nthreads);
  omp_set_num_threads(4);

  printf("static\n");
#pragma omp parallel private(thread_id)
  {
  
    thread_id = omp_get_thread_num();
    nthreads = omp_get_num_threads();

#pragma omp for schedule(static)
    for (int i = 0; i < N; ++i) {
      a[i] = thread_id;
    }

#pragma omp single
    print_usage(a, N, nthreads);
  }

  printf("static with chunks of size 1\n");
#pragma omp parallel private(thread_id)
  {

    thread_id = omp_get_thread_num();
    nthreads = omp_get_num_threads();

#pragma omp for schedule(static, 1) 
    for (int i = 0; i < N; ++i) {
      a[i] = thread_id;
    }

#pragma omp single
    print_usage(a, N, nthreads);
  }
  printf("static with chunks of size 10\n");
#pragma omp parallel private(thread_id)
  {
    thread_id = omp_get_thread_num();
    nthreads = omp_get_num_threads();

#pragma omp for schedule(static, 10)
    for (int i = 0; i < N; ++i) {
      a[i] = thread_id;
    }

#pragma omp single
    print_usage(a, N, nthreads);
  }
  printf("dynamic\n");
#pragma omp parallel private(thread_id)
  {
    thread_id = omp_get_thread_num();
    nthreads = omp_get_num_threads();

#pragma omp for schedule(dynamic)
    for (int i = 0; i < N; ++i) {
      a[i] = thread_id;
    }

#pragma omp single
    print_usage(a, N, nthreads);
  }

  printf("dynamic chunks of size 1\n");
#pragma omp parallel private(thread_id)
  {
    thread_id = omp_get_thread_num();
    nthreads = omp_get_num_threads();

#pragma omp for schedule(dynamic, 1)
    for (int i = 0; i < N; ++i) {
      a[i] = thread_id;
    }

#pragma omp single
    print_usage(a, N, nthreads);
  }

  printf("dynamic chunks of size 10\n");
#pragma omp parallel private(thread_id)
  {
    thread_id = omp_get_thread_num();
    nthreads = omp_get_num_threads();

#pragma omp for schedule(dynamic, 10)
    for (int i = 0; i < N; ++i) {
      a[i] = thread_id;
    }

#pragma omp single
    print_usage(a, N, nthreads);
  }

  return 0;
}
