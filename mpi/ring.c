#include <math.h>
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main(int argc, char *argv[]) {
  int npes = 1;
  int rank = 0;
  MPI_Init(&argc, &argv);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  MPI_Comm_size(MPI_COMM_WORLD, &npes);
  MPI_Request req;
  size_t N = 5;
  int *sum = (int *)malloc(N * sizeof(int));
  int *x = (int *)malloc(N * sizeof(int));
  // int *xr = (int *)malloc(N * sizeof(int));

  for (size_t i = 0; i < N; i++) {
    x[i] = rank;
    // xr[i] = x[i];
    sum[i] = x[i];
  }

  for (size_t i = 0; i < npes; i++) {
    MPI_Send(x, N, MPI_INT, (rank + 1) % npes, 101, MPI_COMM_WORLD);
    for (size_t i = 0; i < N; i++) {
      sum[i] = sum[i] + x[i];
    }
    MPI_Recv(x, N, MPI_INT, (npes + rank - 1) % npes, 101, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
  }

  if (rank == 0) {
    for (size_t i = 0; i < N; i++) {
      printf("%d ", sum[i]);
    }
  }

  MPI_Finalize();
  return 0;
}
