#include <stdio.h>
#include <math.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <unistd.h>

#define N  8192

double seconds()
{
  struct timeval tmp;
  double sec;
  gettimeofday( &tmp, (struct timezone *)0 );
  sec = tmp.tv_sec + ((double)tmp.tv_usec)/1000000.0;
  return sec;

}

__global__ void trans(double *mat_in, double *mat_out)
{
    size_t x=blockIdx.x*blockdim.x+threadIdx.x;
    size_t y=blockIdx.y*blockdim.y+threadIdx.y;
    mat_out[x*N+y]=mat_in[y*N+x];


  }





int main( void ) {
    double *mat_in, *mat_out;            // host copies of a, b, c
    double *mat_in_d, *mat_out_d;
    double time;
    int size = N*N * sizeof( double ); // we need space for 512   									// integers


    // allocate device copies of a, b, c
    cudaMalloc( (void**)&mat_in_d, size );
    cudaMalloc( (void**)&mat_out_d, size );
    mat_in = (double*)malloc( size );
    mat_out = (double*)malloc( size );
    for(int i=0;i<N;i++){
        for(int j=0;j<N;j++){
          mat_in[i*N+j]=(double)rand()/RAND_MAX;

    }}






   // copy inputs to device

   cudaMemcpy(  mat_in_d,mat_in, size, cudaMemcpyHostToDevice );
   time=seconds();
   dim3 grid(256, 256);
   dim3 block(32,32);
   trans<<< grid, block >>>( mat_in_d,mat_out_d);
   cudaDeviceSynchronize();
   time=seconds()-time;
    // copy device result back to host copy of c
   cudaMemcpy( mat_out,  mat_out_d, size, cudaMemcpyDeviceToHost);

    /*for(int i=0;i<10;i++){
       for(int j=0;j<10;j++){

           printf("%lf ",mat_in[i*N+j]);

     }
    printf("\n");
 }

    for(int i=0;i<10;i++){
       for(int j=0;j<10;j++){

           printf("%lf ",mat_out[i*N+j]);

     }
    printf("\n");
 }*/

    dim3 grid1(512, 512);
    dim3 block1(16,16);
    printf("time elapsed using 256 blocks of 1024 threads = %lf\n",time);



    cudaMemcpy( mat_in_d, mat_in, size, cudaMemcpyHostToDevice );
    time=seconds();
    trans<<< grid1, block1 >>>( mat_in_d,mat_out_d);
    cudaDeviceSynchronize();
    time=seconds()-time;
    cudaMemcpy( mat_out_d,  mat_out, size,   cudaMemcpyDeviceToHost );




    printf("time elapsed using 262144 blocks of 256 threads = %lf\n",time);

    dim3 grid2(1024, 1024);
    dim3 block2(8,8);

    cudaMemcpy( mat_in_d, mat_in, size, cudaMemcpyHostToDevice );
    time=seconds();
    trans<<< grid2, block2 >>>( mat_in_d,mat_out_d);
    cudaDeviceSynchronize();
    time=seconds()-time;
    cudaMemcpy( mat_out_d,  mat_out, size,   cudaMemcpyDeviceToHost );


    printf("time elapsed using 2097152 blocks of 64 threads = %lf\n",time);

    free( mat_in); free( mat_out );
    cudaFree( mat_in_d );
    cudaFree( mat_out_d );

    return 0;
}
