#include <omp.h>
#include <stdio.h>
#include <sys/resource.h>
#include <sys/time.h>
#include <unistd.h>

double seconds();
int main() {
  // declarations

  long n, i;
  double sum, inc, h, pi, time;
  // initializations

  n = 1000000000;
  h = 1.0 / n;
  pi = 0;
  sum = 0;
  // for loop to calculate the sum
  time = seconds();
#pragma omp parallel reduction(+ : sum)
  {
    double x;
    // printf("number of threds=%d\n", omp_get_num_threads());
#pragma omp for
    for (i = 1; i < n; i++) {
      x = h * (i - 0.5);
      sum += 4. / (1. + x * x);
    }
  }
  pi = sum * h;
  time = seconds() - time;

  printf("pi=%lf  \n", pi);
  printf("elapsed time is=%f\n", time);
  return 0;
}
double seconds() {
  struct timeval tmp;
  double sec;
  gettimeofday(&tmp, (struct timezone *)0);
  sec = tmp.tv_sec + ((double)tmp.tv_usec) / 1000000.0;
  return sec;
}
