#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>

//#define SIZE 100
#define MPI_TAG 101
double seconds();

int main(int argc, char *argv[]) {

  long n, i;
  double w, x, partial_sum, pi, interval, finalpi, time;
  int rank, size;

  n = 100000000;
  partial_sum = 0.0;
  w = 1.0 / n;

  MPI_Init(&argc, &argv);
  MPI_Comm_size(MPI_COMM_WORLD, &size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
  interval = 1.0 / size;

  // if (rank) printf("size=%i\n",size);
  if (rank == 0)
    time = seconds();
  for (i = rank * n * interval; i <= (rank + 1) * n * interval; i++) {
    x = w * (i - 0.5);
    partial_sum = partial_sum + (4.0 / (1.0 + x * x));
  }

  MPI_Reduce(&partial_sum, &pi, 1, MPI_DOUBLE, MPI_SUM, (size - 1),
             MPI_COMM_WORLD);
  finalpi = w * pi;

  if (rank == size - 1)
    MPI_Send(&finalpi, 1, MPI_DOUBLE, 0, 101, MPI_COMM_WORLD);

  if (rank == 0) {
    MPI_Recv(&finalpi, 1, MPI_DOUBLE, size - 1, 101, MPI_COMM_WORLD,
             MPI_STATUS_IGNORE);
    printf("pi=%f \n", finalpi);
    time = seconds() - time;
    printf("time elapsed in seconds = %f\n", time);
  }
  /*
    if (rank == 1) {
      time = seconds() - time;
      printf("Final pi: %.16g\n", finalpi);
      printf("time elapsed in seconds = %f\n", time);
    }
  */
  MPI_Finalize();
  return 0;
}
double seconds() {

  struct timeval tmp;
  double sec;
  gettimeofday(&tmp, (struct timezone *)0);
  sec = tmp.tv_sec + ((double)tmp.tv_usec) / 1000000.0;
  return sec;
}
